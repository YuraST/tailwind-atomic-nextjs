import { ThemeProvider } from 'next-themes';
import { Header, Footer } from '../ui';

import './styles/tailwind.css';

function MyApp({ Component, pageProps }) {
  return (
    <ThemeProvider attribute="class" enableSystem={false}>
      <Header />
      <Component {...pageProps} />
      <Footer />
    </ThemeProvider>
  );
}

export default MyApp;
