module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './features/**/*.{js,ts,jsx,tsx}',
    './ui/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        'violet': '#947DF1',
        'brand-orange': '#FFBC13',
        'iris': {
          100: '#A71F6E',
          200: '#4D0742',
        },
      }
    },
  },
  plugins: [],
}
