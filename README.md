# Next.js + Tailwind CSS + Atomic Design Principles

## To install the project use the following code.

Make sure you have git and Node installed.

```shell
git clone git@gitlab.com:YuraST/tailwind-atomic-nextjs.git
yarn install
```

To run the server use following command

```shell
yarn dev
```

Then open http://localhost:3000 in your browser

For more run-scripts check `./package.json`

## Description

В основе лежит [данная статья](https://dev.to/nilanth/build-a-portfolio-using-next-js-tailwind-and-vercel-4dd8) с простым примером, как используя Next.js и Tailwind CSS можно собрать простую страничку портфолио. Так же на первом тренинге frontend разработчиков команды ERP рассказал про атомарный дизайн и как его можно применить на React приложение на примере статьи [Structuring your React Application — Atomic Design Principles](https://andela.com/insights/structuring-your-react-application-atomic-design-principles/).
Для большей наглядности применил атомарный дизайн к приложению из [статьи](https://dev.to/nilanth/build-a-portfolio-using-next-js-tailwind-and-vercel-4dd8)

В папке ui храним все основные и переиспользуемые компоненты.
Внутри папки ui находятся папки atoms, molecules и organisms.
