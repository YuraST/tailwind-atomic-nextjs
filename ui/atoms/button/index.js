import React from 'react';

export const Button = ({ children, onClick, ariaLabel, ...props }) => {
  return (
    <button
      {...props}
      aria-label={ariaLabel}
      type="button"
      className="w-8 h-8 p-1 ml-1 mr-1 rounded sm:ml-4"
      onClick={onClick}
    >
      {children}
    </button>
  );
};
