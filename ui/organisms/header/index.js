import React from 'react';
import Link from 'next/link';
import { ThemeSwitch } from '../../molecules';

export const Header = () => (
  <header className="sticky top-0 z-20 py-2 bg-white md:py-6 md:mb-6 dark:bg-black">
    <div className="container px-4 mx-auto lg:max-w-4xl flex items-center justify-between">
      <Link href="/" className={
            'font-medium tracking-wider transition-colors text-black hover:text-sky-500 uppercase dark:text-white'
          }>
          Mars
      </Link>
      <ThemeSwitch />
    </div>
  </header>
);
